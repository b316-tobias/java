package com.zuitt;

import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        Scanner inputLeapYear = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year");
        int leapYear = inputLeapYear.nextInt();

        if ((leapYear % 4 == 0  && leapYear % 100 != 0) || leapYear % 400 == 0) {
            System.out.println(leapYear + " is a leap year");
        } else if (leapYear <= 0) {
            System.out.println("Please enter a valid year");
        } else {
            System.out.println(leapYear + " is NOT a leap year");
        }

    }
}

package com.zuitt;

public class User {

    //Instance
    private String name;
    private int age;
    private String email;
    private String address;




    //constructors
    public User() {

    }

    public User(String name, int age, String email, String address) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.address = address;
    }




    //getter and setter

    //name
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    //age
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    //email
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    //address
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public void message() {
        System.out.println("Hi! I'm " + this.name + ". I'm " + this.age + " years old. You can reach me via my email: " + this.email + ". When I'm off work, I can be found at my house in " + this.address + ".");
    }

}

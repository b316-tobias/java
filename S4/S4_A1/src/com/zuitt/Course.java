package com.zuitt;

public class Course {

    private String name;
    private String description;
    private int seats;
    private double fee;
    private String startDate;
    private String endDate;
    private User instructor;


    //constructors
    public Course() {
        this.instructor = new User();
    }

    public Course(String name,
                  String description,
                  int seats,
                  double fee,
                  String startDate,
                  String endDate,
                  User user) {
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructor = user;
    }


    //getter and setter
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public int getSeats() {
        return seats;
    }
    public void setSeats(int seats) {
        this.seats = seats;
    }

    public double getFee() {
        return fee;
    }
    public void setFee(double fee) {
        this.fee = fee;
    }

    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String startDate) {
        this.endDate = endDate;
    }


    //relationship
    public User getInstructor() {
        return instructor;
    }

    public void  setInstructor(User user) {
        this.instructor = user;
    }

    public String getInstructorName() {
        return this.instructor.getName();
    }

    public void message() {
        System.out.println("Welcome to the " + this.name + " course. This course can be described as " + this.description + " for career-shifters. Which has " + this.seats + " seats available. Reserve your seat now for only " + this.fee + " pesos. Classes will be from " + this.startDate + " until " + this.endDate + ". The instructor for this course is " + this.getInstructorName() + ". Enjoy!");
    }

}

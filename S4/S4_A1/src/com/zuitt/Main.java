package com.zuitt;

import java.util.Date;

public class Main {
    public static void main(String[] args) {

        User user1 = new User();
        user1.setName("Jules");
        user1.setAge(18);
        user1.setEmail("jules@mail.com");
        user1.setAddress("Quezon City");
        user1.message();

        User user2 = new User("Kit", 19, "kit@mail.com", "Laguna");
        user2.message();

        Course course1 = new Course();
        course1.setName("HTML");
        course1.setDescription("Basic front end programming");
        course1.setSeats(20);
        course1.setFee(650.99);
        course1.setStartDate("2023-07-10");
        course1.setEndDate("2023-07-14");


        course1.setInstructor(user2);
        course1.message();

        Course course2 = new Course("CSS", "Basic front end styling", 25, 300.00, "2023-07-10", "2023-07-14", user1);

        course2.message();

    }

}

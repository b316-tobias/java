package com.zuitt;

public class Activity1StretchGoal {
    public static void main(String[] args) {


        for (int row = 1; row <= 5; row++) {
            String line = "";
            for (int asterisk = 1; asterisk <= row; asterisk++) {
                line = line + "* ";
            }
            System.out.println(line);
        }
    }
}

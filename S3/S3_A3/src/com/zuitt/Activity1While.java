package com.zuitt;

import java.util.Scanner;

public class Activity1While {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed");

        int num = 0;
        try{
            num = in.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid Input");
            e.printStackTrace();
        }


        int answer = 1;
        int counter = 1;


        while (counter <= num) {
            answer = counter * answer;
            counter++;
        }

        if (num > 0) {
            System.out.println("The factorial of " + num + " is " + answer);
        } else {
            System.out.println("Please enter a valid positive integer");
        }






    }
}

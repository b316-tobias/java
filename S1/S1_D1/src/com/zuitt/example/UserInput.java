package com.zuitt.example;
import java.util.Scanner;

public class UserInput {
    // add import java.util.Scanner; - as this where the input will be coming from
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in); //Create a Scanner Object
        System.out.print("Enter a username: ");

        String userName = myObj.nextLine(); // read user input
        System.out.println("Username is: " + userName);
    }
}

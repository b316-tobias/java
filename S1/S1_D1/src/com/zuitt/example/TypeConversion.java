package com.zuitt.example;
import java.util.Scanner;
public class TypeConversion {
    public static void main(String[] args) {
        Scanner myObj2 = new Scanner(System.in);
        System.out.print("How old are you?: "); //user input is a string by default

        // option 1
        //double age = new Double(myObj2.nextLine());

        //option 2
        //String age = myObj2.nextLine();
        //double convert_age = Double.parseDouble(age);

        //option 3
        double age = myObj2.nextDouble();

        System.out.println("This is confirmation that you are " + age + " years old.");
    }
}

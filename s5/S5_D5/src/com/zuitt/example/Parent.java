package com.zuitt.example;

public class Parent {

    private String name;
    private int age;

    public Parent() {

    }

    public Parent(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void greet() {
        System.out.println("Hello Friend!");
    }

    //overloading
    public void greet(String name, String timeofDay) {
        System.out.println("Good " + timeofDay + "!, " + name);
    }

    public void speak() {
        System.out.println("I am the parent");
    }
}

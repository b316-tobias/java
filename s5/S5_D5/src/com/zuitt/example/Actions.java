package com.zuitt.example;

//update class to interface
//Interfaces are used to ahieve abstraction. It hides away the actual implementation of the methods and simply shows a "list"/a "menu" of methods that can be done
//Interaces are like blueprints for your classes. Because any class that implemens our interface MUST have the methods in the interface
public interface Actions {
    public void sleep();
    public void run();
    public void eat();
    public void talk();

}

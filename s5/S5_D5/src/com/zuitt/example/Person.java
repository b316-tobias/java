package com.zuitt.example;

//for a class to implement or use an interface, we use the implement keyword
public class Person implements Actions, Greeting {
    public void sleep() {
        System.out.println("zzzzzz......");
    }


    public void run() {
        System.out.println("Running on the road");
    }

    public void eat() {
        System.out.println("Eating on the table");
    }


    public void talk() {
        System.out.println("Kamusta?");
    }

    /*
    * Mini-activity
    * Create 2 new actions that a person would do.
    * */

    public void morningGreeting() {
        System.out.println("Good Morning");
    }

    public void eveningGreeting() {
        System.out.println("Good Evening");
    }
}

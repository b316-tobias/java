package com.zuitt.example;

public interface Greeting {

    public void morningGreeting();
    public void eveningGreeting();
}

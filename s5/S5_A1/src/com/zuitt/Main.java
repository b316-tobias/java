package com.zuitt;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        ArrayList<Contact> contacts = new ArrayList<>();

        Contact contact1 = new Contact("Jules", "+631111111111", "+632222222222", "Quezon City", "Taguig City");
        Contact contact2 = new Contact("Kit", "+63123456789", "+63987654321", "Ilocos", "Laguna");

        contacts.add(contact1);
        contacts.add(contact2);

        Phonebook phonebook = new Phonebook(contacts);

        ArrayList<Contact> retrievedContacts = phonebook.getContacts();



        if (retrievedContacts.isEmpty()) {
            System.out.println("Phonebook is empty");
        } else {

            for (Contact contact: retrievedContacts) {
            System.out.println(contact.getName());
            System.out.println("------------------");
            System.out.println(contact.getName() + " has the following registered numbers:");
            System.out.println(contact.getHomeContactNumber());
            System.out.println(contact.getOfficeContactNumber());
            System.out.println("------------------");
            System.out.println(contact.getName() + " has the following registered addresses:");
            System.out.println("my home in " + contact.getHomeAddress());
            System.out.println("my office in " + contact.getOfficeAddress());
            System.out.println("==========================");
            }
        }

    }
}

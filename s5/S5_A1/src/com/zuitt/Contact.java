package com.zuitt;

public class Contact {

    //variables
    private String name;
    private String homeContactNumber;
    private String officeContactNumber;
    private String homeAddress;
    private String officeAddress;


    //Constructors
    public Contact() {

    }

    public Contact(String name, String homeContactNumber, String officeContactNumber, String homeAddress, String officeAddress) {
        this.name = name;
        this.homeContactNumber = homeContactNumber;
        this.officeContactNumber = officeContactNumber;
        this.homeAddress = homeAddress;
        this.officeAddress = officeAddress;
    }


    //Getters and Setters

    //Setters
    public void setName(String name) {
        this.name = name;
    }
    public void setHomeContactNumber(String contactNumber) {
        this.homeContactNumber = homeContactNumber;
    }
    public void setOfficeContactNumber(String contactNumber) {
        this.officeContactNumber = officeContactNumber;
    }
    public void setHomeAddress(String contactNumber) {
        this.homeAddress = homeAddress;
    }
    public void setOfficeAddress(String address) {
        this.officeAddress = officeAddress;
    }

    //Getters
    public String getName() {
        return name;
    }
    public String getHomeContactNumber() {
        return homeContactNumber;
    }
    public String getOfficeContactNumber() {
        return officeContactNumber;
    }
    public String getHomeAddress() {
        return homeAddress;
    }
    public String getOfficeAddress() {
        return officeAddress;
    }
}
